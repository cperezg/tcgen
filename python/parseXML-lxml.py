import lxml
from lxml import etree
tree = etree.parse("sisgevet.xml")
root = tree.getroot()
print root.nsmap
clases = {}
xmi = '{' + root.nsmap['xmi'] + '}'
for clase in tree.xpath("/uml:Model/packagedElement[@xmi:type='uml:Package']/packagedElement[@xmi:type='uml:Class']", namespaces=root.nsmap):
	clases[clase.attrib[xmi+'id']] = clase.attrib['name']

for clase in tree.xpath("/uml:Model/packagedElement[@xmi:type='uml:Package']/packagedElement[@xmi:type='uml:Class']", namespaces=root.nsmap):
	print "Class: " + clase.attrib['name']
	for attribute in clase.xpath("ownedAttribute[@xmi:type='uml:Property']", namespaces=root.nsmap):
		property_type = attribute.xpath("type", namespaces=root.nsmap)
		attr_type = (property_type[0].attrib["href"].split("#")[1] if len(property_type) > 0 else "")
		if attr_type == "":
			attr_type = clases[attribute.attrib["type"]]
		print "\t" + attribute.attrib['name'] + ':' + attr_type


	for method in clase.xpath("ownedOperation[@xmi:type='uml:Operation']", namespaces=root.nsmap):
		line = "\t" + method.attrib["name"] + "("
		parameters = method.xpath("ownedParameter[@xmi:type='uml:Parameter']", namespaces=root.nsmap)
		for parameter in parameters:
			property_type = parameter.xpath("type", namespaces=root.nsmap)
			attr_type = (property_type[0].attrib["href"].split("#")[1] if len(property_type) > 0 else "")
			if attr_type == "":
				attr_type = clases[parameter.attrib["type"]]
			line += parameter.attrib["name"] + ":" + attr_type
			if parameter != parameters[len(parameters)-1]:
				line += ", "
		line += ")"
		print line

		
for (id, clase) in clases.iteritems():
	print id + ': ' + clase

print "Asociaciones"

for asociacion in root.xpath("packagedElement", namespaces=root.nsmap):
	print asociacion.attrib
