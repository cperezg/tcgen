import org.eclipse.uml2.uml.Model;
import tcgen.*;

import java.util.ArrayList;
import java.util.List;

public class Main {

    private static List<Clase> clases = new ArrayList<>();

    public static void main(String[] args){
        Main main = new Main();
        main.classDiagram();
        main.stateMachine();
    }

    private void classDiagram(){
        Model model = Helper.getModel("sisgevet.uml");
        List<Clase> clases = Helper.getClases(model);

        //XMLCreator.createXML(CriterioAEM.getCriterioAEM(clases), "XML/AEM.xml");
        XMLCreator.createXML(CriterioAEM.getCriterioAEMConcreto(clases), "XML/AEMConcreto.xml");
        //XMLCreator.createXML(CriterioCA.getCriterioCA(clases, 3), "XML/CA.xml");
        XMLCreator.createXML(CriterioCA.getCriterioCAConcreto(clases, 3), "XML/CAConcreto.xml");
        //XMLCreator.createXML(CriterioGN.getCriterioGN(clases), "XML/GN.xml");
        XMLCreator.createXML(CriterioGN.getCriterioGNConcreto(clases), "XML/GNConcreto.xml");
    }

    private void stateMachine(){
        Model model = Helper.getModel("estados.uml");
        //XMLCreator.createXML(CriterioEstados.getCriterioAT(model), "XML/AT.xml");
        XMLCreator.createXML(CriterioEstados.getCriterioATConcreto(model), "XML/ATConcreto.xml");
        //XMLCreator.createXML(CriterioEstados.getCriterioATP(model), "XML/ATP.xml");
        XMLCreator.createXML(CriterioEstados.getCriterioATPConcreto(model), "XML/ATPConcreto.xml");
        //XMLCreator.createXML(CriterioEstados.getCriterioAS(model), "XML/AS.xml");
        XMLCreator.createXML(CriterioEstados.getCriterioASConcreto(model), "XML/ASConcreto.xml");
        //XMLCreator.createXML(CriterioEstados.getCriterioALFP(model), "XML/ALFP.xml");
        XMLCreator.createXML(CriterioEstados.getCriterioALFPConcreto(model), "XML/ALFPConcreto.xml");
    }



}