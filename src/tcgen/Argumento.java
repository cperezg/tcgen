package tcgen;

public class Argumento extends Atributo {
    private int orden;

    public Argumento(String nombre, String tipo, int orden) {
        super(nombre, tipo);
        this.orden = orden;
    }

    public void setOrden(int orden) {

        this.orden = orden;
    }

    public int getOrden() {

        return orden;
    }
}
