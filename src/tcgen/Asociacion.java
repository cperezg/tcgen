package tcgen;

public class Asociacion {
    private Clase claseDestino;
    private int multiplicidadDesde;

    public Asociacion(Clase claseDestino, int multiplicidadDesde, int getMultiplicidadHasta) {
        this.claseDestino = claseDestino;
        this.multiplicidadDesde = multiplicidadDesde;
        this.getMultiplicidadHasta = getMultiplicidadHasta;
    }

    private int getMultiplicidadHasta;

    public Clase getClaseDestino() {
        return claseDestino;
    }

    public void setClaseDestino(Clase claseDestino) {
        this.claseDestino = claseDestino;
    }

    public int getMultiplicidadDesde() {
        return multiplicidadDesde;
    }

    public void setMultiplicidadDesde(int multiplicidadDesde) {
        this.multiplicidadDesde = multiplicidadDesde;
    }

    public int getGetMultiplicidadHasta() {
        return getMultiplicidadHasta;
    }

    public void setGetMultiplicidadHasta(int getMultiplicidadHasta) {
        this.getMultiplicidadHasta = getMultiplicidadHasta;
    }
}
