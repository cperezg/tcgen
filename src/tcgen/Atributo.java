package tcgen;

import org.fluttercode.datafactory.impl.DataFactory;

import javax.xml.crypto.Data;

public class Atributo {
    private String nombre;
    private String tipo;
    private String valor;

    public String getValor() {

        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public Atributo(String nombre, String tipo) {
        this.nombre = nombre;
        this.tipo = tipo;
    }


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
}
