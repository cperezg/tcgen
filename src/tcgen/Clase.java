package tcgen;

import java.util.ArrayList;
import java.util.List;

public class Clase {
    private String nombre;
    private List<Atributo> atributos;
    private List<Metodo> metodos;
    private List<Asociacion> asociaciones;

    private List<Clase> clasesHijas;

    public Clase() {
        this.nombre = new String();
        this.atributos = new ArrayList<>();
        this.metodos = new ArrayList<>();
        this.asociaciones = new ArrayList<>();
        this.clasesHijas = new ArrayList<>();
    }

    public List<Asociacion> getAsociaciones() {
        return asociaciones;
    }

    public void setAsociaciones(List<Asociacion> asociaciones) {
        this.asociaciones = asociaciones;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<Atributo> getAtributos() {
        return atributos;
    }

    public void setAtributos(List<Atributo> atributos) {
        this.atributos = atributos;
    }

    public List<Metodo> getMetodos() {
        return metodos;
    }

    public void setMetodos(List<Metodo> metodos) {
        this.metodos = metodos;
    }

    public List<Clase> getClasesHijas() {
        return clasesHijas;
    }

    public void setClasesHijas(List<Clase> clasesHijas) {
        this.clasesHijas = clasesHijas;
    }

    public void addClaseHija(Clase claseHija){
        this.clasesHijas.add(claseHija);
        return;
    }

}

