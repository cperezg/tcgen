package tcgen;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.eclipse.uml2.uml.Model;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Controller {
    private Stage stage;
    private List<Clase> clases;
    private Model modeloEstados;
    private String output = "";
    private Document outputXML;
    private List<Model> modelosEstados = new ArrayList<>();
    private List<String> modelosEstadosArchivos = new ArrayList<>();

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    private void handleCriteriaCheckboxAction(ActionEvent event) {
        /*
        CheckBox checkbox = (CheckBox) event.getTarget();

        if (((CheckBox)event.getSource()).getParent().getParent().getId().equals("class_criterias")){
            CheckBox selectAll = (CheckBox) getStage().getScene().lookup("#select_all_class");
            if (checkbox.isSelected()){
                if (selectAll.isSelected()){

                }
            } else {

            }
        } else {

        }
        */
    }

    @FXML
    private void handleClose(ActionEvent event) {
        Platform.exit();
    }
    
    @FXML
    private void handleLoadClassUml(ActionEvent event){
        File umlFile = getFileFromChooser();
        if (umlFile == null)
            return;

        try {
            clases = Helper.getClases(Helper.getModel(umlFile.getAbsolutePath()));
        } catch (Exception e){
            Alert alert = new Alert(Alert.AlertType.ERROR, "Digrama incorrecto", ButtonType.CLOSE);
            alert.show();
            return;
        }

        stage.getScene().lookup("#load_classes_button").setDisable(true);
        stage.getScene().lookup("#generar_button").setDisable(false);

        TitledPane titledPane = (TitledPane)stage.getScene().lookup("#class_criterias");
        List<CheckBox> criterias = ((VBox)titledPane.getContent()).getChildren().stream().filter(c -> c.getClass() == CheckBox.class).map(c -> (CheckBox)c).collect(Collectors.toList());
        for (CheckBox criteria : criterias){
            criteria.setDisable(false);
        }
    }

    @FXML
    private void handleLoadStatesUml(ActionEvent event){
        File umlFile = getFileFromChooser();
        if (umlFile == null)
            return;

        try {
            modelosEstados.add(Helper.getModel(umlFile.getAbsolutePath()));
            modelosEstadosArchivos.add(umlFile.getName());
        } catch (Exception e){
            Alert alert = new Alert(Alert.AlertType.ERROR, "Error", ButtonType.CLOSE);
            alert.show();
            return;
        }


        //stage.getScene().lookup("#load_states_button").setDisable(true);
        stage.getScene().lookup("#generar_button").setDisable(false);
        String listaArchivosText = "Maquinas de estado:\n";
        for (String filename : modelosEstadosArchivos){
            listaArchivosText += filename + "\n";
        }
        ((TextArea)getStage().getScene().lookup("#lista_archivos")).setText(listaArchivosText);
        TitledPane titledPane = (TitledPane)stage.getScene().lookup("#states_criterias");
        List<CheckBox> criterias = ((VBox)titledPane.getContent()).getChildren().stream().filter(c -> c.getClass() == CheckBox.class).map(c -> (CheckBox)c).collect(Collectors.toList());
        for (CheckBox criteria : criterias){
            criteria.setDisable(false);
        }
    }

    @FXML
    private void handleGenerarCasos(ActionEvent event){
        long tiempo_inicial = System.currentTimeMillis();
        output = "";
        getStage().getScene().lookup("#exportar_button").setDisable(false);

        try {
            outputXML = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }

        Element rootElement = outputXML.createElement("suite");
        rootElement.setAttribute("name", "Test Sisgevet");
        rootElement.setAttribute("verbose", "10");

        /* parameters */
        Element parameterHost = outputXML.createElement("parameter");
        parameterHost.setAttribute("name", "selenium.host");
        parameterHost.setAttribute("value", "localhost");
        rootElement.appendChild(parameterHost);

        Element parameterPort = outputXML.createElement("parameter");
        parameterPort.setAttribute("name", "selenium.port");
        parameterPort.setAttribute("value", "3737");
        rootElement.appendChild(parameterPort);

        Element parameterBrowser = outputXML.createElement("parameter");
        parameterBrowser.setAttribute("name", "selenium.browser");
        parameterBrowser.setAttribute("value", "firefox");
        rootElement.appendChild(parameterBrowser);

        Element parameterUrl = outputXML.createElement("parameter");
        parameterUrl.setAttribute("name", "selenium.url");
        parameterUrl.setAttribute("value", "http://www.sisgevet.com");
        rootElement.appendChild(parameterUrl);

        Element test = outputXML.createElement("test");
        List<CheckBox> checkboxes = ((VBox)((TitledPane)getStage().getScene().lookup("#class_criterias")).getContent()).getChildren().stream().filter(c -> c.getClass() == CheckBox.class && !c.getId().equals("select_all_class")).map(c -> (CheckBox)c).collect(Collectors.toList());
        checkboxes.addAll(((VBox)((TitledPane)getStage().getScene().lookup("#states_criterias")).getContent()).getChildren().stream().filter(c -> c.getClass() == CheckBox.class && !c.getId().equals("select_all_state")).map(c -> (CheckBox)c).collect(Collectors.toList()));


        XMLCreator.nCasos = 0;
        for (CheckBox criteriaCheckbox : checkboxes){
            if (!criteriaCheckbox.isSelected() || criteriaCheckbox.isDisabled())
                continue;
            String criteria = criteriaCheckbox.getId();
            switch (criteria){
                case "checkbox_gn":
                    test.appendChild(CriterioGN.getCriterioGN(clases, outputXML));
                    break;
                case "checkbox_aem":
                    test.appendChild(CriterioAEM.getCriterioAEM(clases, outputXML));
                    break;
                case "checkbox_ca":
                    test.appendChild(CriterioCA.getCriterioCA(clases, 2, outputXML));
                    break;
                case "checkbox_as":
                    /* Iterar los modelos de estados */
                    for (Model modelo : modelosEstados){
                        test.appendChild(CriterioEstados.getCriterioAS(modelo, outputXML));
                    }
                    //test.appendChild(CriterioEstados.getCriterioAS(modeloEstados, outputXML));
                    break;
                case "checkbox_at":
                    for (Model modelo : modelosEstados) {
                        test.appendChild(CriterioEstados.getCriterioAT(modelo, outputXML));
                    }
                    //test.appendChild(CriterioEstados.getCriterioAT(modeloEstados, outputXML));
                    break;
                case "checkbox_atp":
                    for (Model modelo : modelosEstados){
                        test.appendChild(CriterioEstados.getCriterioATP(modelo, outputXML));
                    }
                    //test.appendChild(CriterioEstados.getCriterioATP(modeloEstados, outputXML));
                    break;
                case "checkbox_alfp":
                    for (Model modelo : modelosEstados){
                        test.appendChild(CriterioEstados.getCriterioALFP(modelo, outputXML));
                    }
                    //test.appendChild(CriterioEstados.getCriterioALFP(modeloEstados, outputXML));
                    break;
            }
        }
        rootElement.appendChild(test);
        outputXML.appendChild(rootElement);
        try {
            output = XMLCreator.getXML(outputXML);
        } catch (TransformerException e) {
            e.printStackTrace();
        }

        long tiempo_final = System.currentTimeMillis();
        output = "Total Time: " + (tiempo_final - tiempo_inicial)*1.0/1000 + " seconds.\n\nNumber of Test Cases: " + XMLCreator.nCasos + "\n\n" + output;
        ((TextArea)getStage().getScene().lookup("#output_area")).setText(output);
    }

    @FXML
    private void handleExportarCasos(ActionEvent event){
        FileChooser outputFileChooser = new FileChooser();
        outputFileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("XML", "*.xml")
        );
        outputFileChooser.setTitle("Exportar Casos de Prueba");
        outputFileChooser.setInitialDirectory(new File(System.getProperty("user.dir")));
        File outputFile = outputFileChooser.showSaveDialog(stage);
        if (outputFile == null)
            return;
        XMLCreator.createXML(outputXML, outputFile.getAbsolutePath());
    }

    @FXML
    private void handleSelectAllCheckbox(ActionEvent event){
        CheckBox selectAll = (CheckBox)event.getSource();
        if (selectAll.getId().equals("select_all_class")){
            TitledPane titledPane = (TitledPane)stage.getScene().lookup("#class_criterias");
            List<CheckBox> criterias = ((VBox)titledPane.getContent()).getChildren().stream().filter(c -> c.getClass() == CheckBox.class && !c.getId().equals("select_all_class")).map(c -> (CheckBox)c).collect(Collectors.toList());
            if (selectAll.isSelected()){
                for (CheckBox criteria : criterias){
                    criteria.setSelected(true);
                }
            } else {
                for (CheckBox criteria : criterias){
                    criteria.setSelected(false);
                }
            }
        } else {
            TitledPane titledPane = (TitledPane)stage.getScene().lookup("#states_criterias");
            List<CheckBox> criterias = ((VBox)titledPane.getContent()).getChildren().stream().filter(c -> c.getClass() == CheckBox.class && !c.getId().equals("select_all_state")).map(c -> (CheckBox)c).collect(Collectors.toList());
            if (selectAll.isSelected()){
                for (CheckBox criteria : criterias){
                    criteria.setSelected(true);
                }
            } else {
                for (CheckBox criteria : criterias){
                    criteria.setSelected(false);
                }
            }
        }
    }

    private File getFileFromChooser(){
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("UML", "*.uml"),
                new FileChooser.ExtensionFilter("XML", "*.xml")
        );
        fileChooser.setTitle("Abrir un modelo uml");
        fileChooser.setInitialDirectory(new File(System.getProperty("user.dir")));
        return fileChooser.showOpenDialog(stage);
    }

}
