package tcgen;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.util.ArrayList;
import java.util.List;

public class CriterioAEM {
    private DocumentBuilder docBuilder;

    public CriterioAEM(){
        try{
            docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        }
    }

    public static Element getCriterioAEM(List<Clase> clases, Document doc){

        Element aem = doc.createElement("Criterio");
        aem.setAttribute("nombre", "AEM");
        for (Clase cls : clases) {
            if (cls.getAsociaciones().size() == 0) continue;
            Element criterio = doc.createElement("Clase");
            criterio.setAttribute("nombre", cls.getNombre());

            for (Asociacion asociacion : cls.getAsociaciones()) {
                Element agregacionValida = doc.createElement("Agregacion");
                agregacionValida.setAttribute("nombre", asociacion.getClaseDestino().getNombre() + cls.getNombre());
                agregacionValida.setAttribute("claseOrigen", cls.getNombre());
                agregacionValida.setAttribute("claseDestino", asociacion.getClaseDestino().getNombre());
                agregacionValida.setAttribute("valida", "true");

                List<Integer> multiA = getMultiplicidadesValidas(asociacion.getMultiplicidadDesde());
                List<Integer> multiB = getMultiplicidadesValidas(asociacion.getGetMultiplicidadHasta());
                for (Integer a : multiA) {
                    for (Integer b : multiB) {
                        XMLCreator.nCasos++;
                        Element multiplicidad = doc.createElement("Multiplicidad");
                        multiplicidad.setAttribute("cardinalidad", a + "," + b);
                        for (int i = 0; i < a; i++){
                            multiplicidad.appendChild(Helper.crearInstancia(doc, cls));
                        }
                        for (int i = 0; i < b; i++){
                            multiplicidad.appendChild(Helper.crearInstancia(doc, asociacion.getClaseDestino()));
                        }
                        agregacionValida.appendChild(multiplicidad);
                    }
                }
                criterio.appendChild(agregacionValida);

                Element agregacionInvalida = doc.createElement("Agregacion");
                agregacionInvalida.setAttribute("nombre", asociacion.getClaseDestino().getNombre() + cls.getNombre());
                agregacionInvalida.setAttribute("claseOrigen", cls.getNombre());
                agregacionInvalida.setAttribute("claseDestino", asociacion.getClaseDestino().getNombre());
                agregacionInvalida.setAttribute("valida", "false");

                multiA = getMultiplicidadesInvalidas(asociacion.getMultiplicidadDesde());
                multiB = getMultiplicidadesInvalidas(asociacion.getGetMultiplicidadHasta());
                for (Integer a : multiA) {
                    for (Integer b : multiB) {
                        Element multiplicidad = doc.createElement("Multiplicidad");
                        multiplicidad.setAttribute("cardinalidad", a + "," + b);
                        for (int i = 0; i < a; i++){
                            multiplicidad.appendChild(Helper.crearInstancia(doc, cls));
                        }
                        for (int i = 0; i < b; i++){
                            multiplicidad.appendChild(Helper.crearInstancia(doc, asociacion.getClaseDestino()));
                        }
                        agregacionInvalida.appendChild(multiplicidad);
                    }
                }
                criterio.appendChild(agregacionInvalida);
            }
            aem.appendChild(criterio);
        }

        return aem;
    }

    public static Document getCriterioAEMConcreto(List<Clase> clases){
        CriterioAEM criterioAEM = new CriterioAEM();
        Document doc = criterioAEM.docBuilder.newDocument();

        Element rootElement = doc.createElement("suite");
        rootElement.setAttribute("name", "Test Sisgevet");
        rootElement.setAttribute("verbose", "10");

        /* parameters */
        Element parameterHost = doc.createElement("parameter");
        parameterHost.setAttribute("name", "selenium.host");
        parameterHost.setAttribute("value", "localhost");
        rootElement.appendChild(parameterHost);

        Element parameterPort = doc.createElement("parameter");
        parameterPort.setAttribute("name", "selenium.port");
        parameterPort.setAttribute("value", "3737");
        rootElement.appendChild(parameterPort);

        Element parameterBrowser = doc.createElement("parameter");
        parameterBrowser.setAttribute("name", "selenium.browser");
        parameterBrowser.setAttribute("value", "firefox");
        rootElement.appendChild(parameterBrowser);

        Element parameterUrl = doc.createElement("parameter");
        parameterUrl.setAttribute("name", "selenium.url");
        parameterUrl.setAttribute("value", "http://www.sisgevet.com");
        rootElement.appendChild(parameterUrl);

        Element test = doc.createElement("test");

        for (Clase cls : clases) {
            if (cls.getAsociaciones().size() == 0) continue;
            Element criterio = doc.createElement("Criterio");
            criterio.setAttribute("nombre", "AEM");
            criterio.setAttribute("clase", cls.getNombre());

            for (Asociacion asociacion : cls.getAsociaciones()) {
                Element agregacionValida = doc.createElement("Agregacion");
                agregacionValida.setAttribute("nombre", asociacion.getClaseDestino().getNombre() + cls.getNombre());
                agregacionValida.setAttribute("claseOrigen", cls.getNombre());
                agregacionValida.setAttribute("claseDestino", asociacion.getClaseDestino().getNombre());
                agregacionValida.setAttribute("valida", "true");

                List<Integer> multiA = getMultiplicidadesValidas(asociacion.getMultiplicidadDesde());
                List<Integer> multiB = getMultiplicidadesValidas(asociacion.getGetMultiplicidadHasta());
                for (Integer a : multiA) {
                    for (Integer b : multiB) {
                        Element multiplicidad = doc.createElement("Multiplicidad");
                        multiplicidad.setAttribute("cardinalidad", a + "," + b);
                        for (int i = 0; i < a; i++){
                            multiplicidad.appendChild(Helper.crearInstancia(doc, cls));
                        }
                        for (int i = 0; i < b; i++){
                            multiplicidad.appendChild(Helper.crearInstancia(doc, asociacion.getClaseDestino()));
                        }
                        agregacionValida.appendChild(multiplicidad);
                    }
                }
                criterio.appendChild(agregacionValida);

                Element agregacionInvalida = doc.createElement("Agregacion");
                agregacionInvalida.setAttribute("nombre", asociacion.getClaseDestino().getNombre() + cls.getNombre());
                agregacionInvalida.setAttribute("claseOrigen", cls.getNombre());
                agregacionInvalida.setAttribute("claseDestino", asociacion.getClaseDestino().getNombre());
                agregacionInvalida.setAttribute("valida", "false");

                multiA = getMultiplicidadesInvalidas(asociacion.getMultiplicidadDesde());
                multiB = getMultiplicidadesInvalidas(asociacion.getGetMultiplicidadHasta());
                for (Integer a : multiA) {
                    for (Integer b : multiB) {
                        XMLCreator.nCasos++;
                        Element multiplicidad = doc.createElement("Multiplicidad");
                        multiplicidad.setAttribute("cardinalidad", a + "," + b);
                        for (int i = 0; i < a; i++){
                            multiplicidad.appendChild(Helper.crearInstancia(doc, cls));
                        }
                        for (int i = 0; i < b; i++){
                            multiplicidad.appendChild(Helper.crearInstancia(doc, asociacion.getClaseDestino()));
                        }
                        agregacionInvalida.appendChild(multiplicidad);
                    }
                }
                criterio.appendChild(agregacionInvalida);
            }
            test.appendChild(criterio);
            rootElement.appendChild(test);
        }

        doc.appendChild(rootElement);
        return doc;
    }

    private static ArrayList<Integer> getMultiplicidadesValidas(Integer multiplicidad) {
        ArrayList<Integer> lista = new ArrayList<>();
        if (multiplicidad == 0 || multiplicidad == 1) {
            lista.add(multiplicidad);
        } else if (multiplicidad == -1) {
            lista.add(0);
            lista.add(1);
            lista.add(5);
        }
        return lista;
    }

    private static ArrayList<Integer> getMultiplicidadesInvalidas(Integer multiplicidad) {
        ArrayList<Integer> lista = new ArrayList<>();
        if (multiplicidad == 1) {
            lista.add(0);
            lista.add(2);
            lista.add(5);
        } else if (multiplicidad == -1) {
            lista.add(1);
        }
        return lista;
    }


}
