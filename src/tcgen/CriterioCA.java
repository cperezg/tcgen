package tcgen;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.util.ArrayList;
import java.util.List;

public class CriterioCA {
    private DocumentBuilder docBuilder;
    private int casosPorAtributo;

    public CriterioCA(int casosPorAtributo) {
        this.casosPorAtributo = casosPorAtributo;
        try {
            docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        }
    }

    public Element getCriterioCA(List<Clase> clases, Document doc){
        Element ca = doc.createElement("Criterio");
        ca.setAttribute("nombre", "CA");

        for (Clase clase : clases){
            Element criterio = doc.createElement("Clase");
            criterio.setAttribute("nombre", clase.getNombre());
            ArrayList<ArrayList<Atributo>> listaAtributos = new ArrayList<>();
            for (Atributo atributo : clase.getAtributos()){
                ArrayList<Atributo> lAtributo = new ArrayList<>();
                for (int i = 0; i < casosPorAtributo; i++){
                    Atributo atr = new Atributo(atributo.getNombre(), atributo.getTipo());
                    atr.setValor(Helper.getValor(atributo.getNombre(), atributo.getTipo()));
                    lAtributo.add(atr);
                }
                listaAtributos.add(lAtributo);
            }
            List<List<Atributo>> productoCartesiano = Helper.cartesianProduct(listaAtributos);
            for (List<Atributo> atributos : productoCartesiano){
                XMLCreator.nCasos++;
                Element instancia = doc.createElement("Instancia");
                for (Atributo atributo : atributos){
                    Element atr = doc.createElement("Atributo");
                    atr.setAttribute("nombre", atributo.getNombre());
                    atr.setAttribute("tipo", atributo.getTipo());
                    atr.setAttribute("valor", atributo.getValor());
                    instancia.appendChild(atr);
                }
                criterio.appendChild(instancia);
            }
            ca.appendChild(criterio);
        }

        return ca;
    }

    public Document getCriterioCAConcreto(List<Clase> clases){
        Document doc = docBuilder.newDocument();

        Element rootElement = doc.createElement("suite");
        rootElement.setAttribute("name", "Test Sisgevet");
        rootElement.setAttribute("verbose", "10");

        /* parameters */
        Element parameterHost = doc.createElement("parameter");
        parameterHost.setAttribute("name", "selenium.host");
        parameterHost.setAttribute("value", "localhost");
        rootElement.appendChild(parameterHost);

        Element parameterPort = doc.createElement("parameter");
        parameterPort.setAttribute("name", "selenium.port");
        parameterPort.setAttribute("value", "3737");
        rootElement.appendChild(parameterPort);

        Element parameterBrowser = doc.createElement("parameter");
        parameterBrowser.setAttribute("name", "selenium.browser");
        parameterBrowser.setAttribute("value", "firefox");
        rootElement.appendChild(parameterBrowser);

        Element parameterUrl = doc.createElement("parameter");
        parameterUrl.setAttribute("name", "selenium.url");
        parameterUrl.setAttribute("value", "http://www.sisgevet.com");
        rootElement.appendChild(parameterUrl);

        Element test = doc.createElement("test");

        for (Clase clase : clases){
            XMLCreator.nCasos++;
            Element criterio = doc.createElement("Criterio");
            criterio.setAttribute("nombre", "CA");
            criterio.setAttribute("clase", clase.getNombre());
            ArrayList<ArrayList<Atributo>> listaAtributos = new ArrayList<>();
            for (Atributo atributo : clase.getAtributos()){
                ArrayList<Atributo> lAtributo = new ArrayList<>();
                for (int i = 0; i < casosPorAtributo; i++){
                    Atributo atr = new Atributo(atributo.getNombre(), atributo.getTipo());
                    atr.setValor(Helper.getValor(atributo.getNombre(), atributo.getTipo()));
                    lAtributo.add(atr);
                }
                listaAtributos.add(lAtributo);
            }
            List<List<Atributo>> productoCartesiano = Helper.cartesianProduct(listaAtributos);
            for (List<Atributo> atributos : productoCartesiano){
                Element instancia = doc.createElement("Instancia");
                for (Atributo atributo : atributos){
                    Element atr = doc.createElement("Atributo");
                    atr.setAttribute("nombre", atributo.getNombre());
                    atr.setAttribute("tipo", atributo.getTipo());
                    atr.setAttribute("valor", atributo.getValor());
                    instancia.appendChild(atr);
                }
                criterio.appendChild(instancia);
            }
            test.appendChild(criterio);
            rootElement.appendChild(test);
        }
        doc.appendChild(rootElement);
        return doc;
    }

    public static Element getCriterioCA(List<Clase> clases, int casosPorAtributo, Document doc){
        CriterioCA criterioCA = new CriterioCA(casosPorAtributo);
        return criterioCA.getCriterioCA(clases, doc);
    }

    public static Document getCriterioCAConcreto(List<Clase> clases, int casosPorAtributo){
        CriterioCA criterioCA = new CriterioCA(casosPorAtributo);
        return criterioCA.getCriterioCAConcreto(clases);
    }
}
