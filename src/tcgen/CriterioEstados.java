package tcgen;

import org.eclipse.uml2.uml.*;
import org.eclipse.uml2.uml.internal.impl.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.util.*;
import java.util.stream.Collectors;

public class CriterioEstados {
    private DocumentBuilder docBuilder;

    public CriterioEstados() {
        try {
            docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        }
    }

    public static Element getCriterioATP(Model modelo, Document doc){
        Element allTransitionsPair = doc.createElement("Criterio");
        allTransitionsPair.setAttribute("nombre", "All-Transitions-Pair");

        StateMachine stateMachine = (StateMachine)modelo.getPackagedElements().get(0);
        Region region = stateMachine.getRegions().get(0);
        String regionName = stateMachine.getName();
        allTransitionsPair.setAttribute("diagrama", regionName);
        Pseudostate estadoInicial = (Pseudostate) region.getSubvertices().stream().filter(s -> s.getClass() == PseudostateImpl.class && ((Pseudostate)s).getKind().getName() == "initial").findFirst().get();

        Map<Integer, Vertex> estados = new HashMap<>();
        for (Vertex estado : region.getSubvertices()){
            estados.put(estado.hashCode(), estado);
        }

        for (Vertex estado : estados.entrySet().stream().map(s -> s.getValue()).collect(Collectors.toList())){
            if (estado.getClass() == PseudostateImpl.class || estado.getClass() == FinalStateImpl.class) {
                continue;
            } else {
                Element caso = doc.createElement("Estado");
                caso.setAttribute("nombre", estado.getLabel());
                List<Transition> transicionesEntrantes = estado.getIncomings();
                List<Transition> transicionesSalientes = estado.getOutgoings();
                for (Transition entrante : transicionesEntrantes) {
                    for (Transition saliente : transicionesSalientes) {
                        if (entrante.getLabel() == saliente.getLabel())
                            continue;
                        Element transicion = doc.createElement("Transicion");
                        transicion.setAttribute("transicionEntrante", entrante.getLabel());
                        transicion.setAttribute("transicionSaliente", saliente.getLabel());
                        transicion.setAttribute("estado", estado.getLabel());
                        caso.appendChild(transicion);
                    }
                }
                if (caso.getChildNodes().getLength() > 0)
                    allTransitionsPair.appendChild(caso);
                else {
                    Element casoFallido = doc.createElement("EstadoFallido");
                    casoFallido.setAttribute("nombre", estado.getLabel());
                    allTransitionsPair.appendChild(casoFallido);
                }

            }
        }
        return allTransitionsPair;
    }

    public static Document getCriterioATPConcreto(Model modelo){
        CriterioEstados criterioEstados = new CriterioEstados();
        Document doc = criterioEstados.docBuilder.newDocument();

        Element rootElement = doc.createElement("suite");
        rootElement.setAttribute("name", "Test Sisgevet");
        rootElement.setAttribute("verbose", "10");

        /* parameters */
        Element parameterHost = doc.createElement("parameter");
        parameterHost.setAttribute("name", "selenium.host");
        parameterHost.setAttribute("value", "localhost");
        rootElement.appendChild(parameterHost);

        Element parameterPort = doc.createElement("parameter");
        parameterPort.setAttribute("name", "selenium.port");
        parameterPort.setAttribute("value", "3737");
        rootElement.appendChild(parameterPort);

        Element parameterBrowser = doc.createElement("parameter");
        parameterBrowser.setAttribute("name", "selenium.browser");
        parameterBrowser.setAttribute("value", "firefox");
        rootElement.appendChild(parameterBrowser);

        Element parameterUrl = doc.createElement("parameter");
        parameterUrl.setAttribute("name", "selenium.url");
        parameterUrl.setAttribute("value", "http://www.sisgevet.com");
        rootElement.appendChild(parameterUrl);

        Element test = doc.createElement("test");

        Element allTransitionsPair = doc.createElement("Criterio");
        allTransitionsPair.setAttribute("nombre", "All-Transitions-Pair");

        StateMachine stateMachine = (StateMachine)modelo.getPackagedElements().get(0);
        Region region = stateMachine.getRegions().get(0);
        Pseudostate estadoInicial = (Pseudostate) region.getSubvertices().stream().filter(s -> s.getClass() == PseudostateImpl.class && ((Pseudostate)s).getKind().getName() == "initial").findFirst().get();

        Map<Integer, Vertex> estados = new HashMap<>();
        for (Vertex estado : region.getSubvertices()){
            estados.put(estado.hashCode(), estado);
        }

        for (Vertex estado : estados.entrySet().stream().map(s -> s.getValue()).collect(Collectors.toList())){
            if (estado.getClass() == PseudostateImpl.class || estado.getClass() == FinalStateImpl.class) {
                continue;
            } else {
                Element caso = doc.createElement("Estado");
                caso.setAttribute("nombre", estado.getLabel());
                List<Transition> transicionesEntrantes = estado.getIncomings();
                List<Transition> transicionesSalientes = estado.getOutgoings();
                for (Transition entrante : transicionesEntrantes) {
                    for (Transition saliente : transicionesSalientes) {
                        if (entrante.getLabel() == saliente.getLabel())
                            continue;
                        Element transicion = doc.createElement("Transicion");
                        transicion.setAttribute("transicionEntrante", entrante.getLabel());
                        transicion.setAttribute("transicionSaliente", saliente.getLabel());
                        transicion.setAttribute("estado", estado.getLabel());
                        caso.appendChild(transicion);
                    }
                }
                if (caso.getChildNodes().getLength() > 0)
                    allTransitionsPair.appendChild(caso);
                else {
                    Element casoFallido = doc.createElement("EstadoFallido");
                    casoFallido.setAttribute("nombre", estado.getLabel());
                    allTransitionsPair.appendChild(casoFallido);
                }

            }
        }
        test.appendChild(allTransitionsPair);
        rootElement.appendChild(test);
        doc.appendChild(rootElement);
        return doc;
    }

    public static Element getCriterioAT(Model modelo, Document doc){
        Element allTransitions = doc.createElement("Criterio");
        allTransitions.setAttribute("nombre", "All-Transitions");

        StateMachine stateMachine = (StateMachine)modelo.getPackagedElements().get(0);
        Region region = stateMachine.getRegions().get(0);
        String regionName = stateMachine.getName();
        allTransitions.setAttribute("diagrama", regionName);
        Pseudostate estadoInicial = (Pseudostate) region.getSubvertices().stream().filter(s -> s.getClass() == PseudostateImpl.class && ((Pseudostate)s).getKind().getName() == "initial").findFirst().get();

        Map<Integer, Vertex> estados = new HashMap<>();
        Map<Integer, Boolean> visitados = new HashMap<>();
        for (Vertex estado : region.getSubvertices()){
            estados.put(estado.hashCode(), estado);
            visitados.put(estado.hashCode(), false);
        }
        visitados.put(estadoInicial.hashCode(), true);

        Map<Integer, Transition> transiciones = new HashMap<>();
        Map<Integer, Boolean> transicionesVisitadas = new HashMap<>();
        for( Transition transicion : region.getTransitions()){
            transiciones.put(transicion.hashCode(), transicion);
            transicionesVisitadas.put(transicion.hashCode(), false);
        }

        List<NamedElement> pasos = new ArrayList<>();
        pasos.add(estadoInicial);
        List<Transition> transicionesPendientes = new ArrayList<>();
        transicionesPendientes.addAll(estadoInicial.getOutgoings());

        while(!transicionesPendientes.isEmpty()){
            Transition transicion = transicionesPendientes.remove(transicionesPendientes.size()-1);

            Vertex estado = transicion.getTarget();

            pasos.add(transicion);

            transicionesVisitadas.put(transicion.hashCode(), true);
            if (!visitados.get(estado.hashCode())){
                visitados.put(estado.hashCode(), true);
                transicionesPendientes.addAll(estado.getOutgoings());
            }

            if (estado.getClass() == FinalStateImpl.class){
                XMLCreator.nCasos++;
                Element caso = doc.createElement("Caso");
                caso.setAttribute("estadoFinal", estado.getLabel());
                for (NamedElement paso : pasos){
                    if (paso.getClass() == TransitionImpl.class){
                        Element p = doc.createElement("Transicion");
                        p.setAttribute("nombre", paso.getLabel());
                        p.setAttribute("origen", ((Transition)paso).getSource().getLabel());
                        p.setAttribute("destino", ((Transition)paso).getTarget().getLabel());
                        caso.appendChild(p);
                    }
                }
                allTransitions.appendChild(caso);
                pasos.remove(pasos.size()-1);
            } else {
                if (estado.getOutgoings().size() == 0)
                    visitados.put(estado.hashCode(), false);
            }
        }

        Element noVisitadosXml = doc.createElement("TransicionesNoVisitadas");
        for (Integer hashKey : transicionesVisitadas.entrySet().stream().filter(v -> !v.getValue()).map(v -> v.getKey()).collect(Collectors.toList())){
            Transition transicion = transiciones.get(hashKey);
            Element noVisitado = doc.createElement("Transicion");
            noVisitado.setAttribute("nombre", transicion.getLabel());
            noVisitadosXml.appendChild(noVisitado);
        }
        allTransitions.appendChild(noVisitadosXml);

        return allTransitions;
    }

    public static Document getCriterioATConcreto(Model modelo){
        CriterioEstados criterioEstados = new CriterioEstados();
        Document doc = criterioEstados.docBuilder.newDocument();

        Element rootElement = doc.createElement("suite");
        rootElement.setAttribute("name", "Test Sisgevet");
        rootElement.setAttribute("verbose", "10");

        /* parameters */
        Element parameterHost = doc.createElement("parameter");
        parameterHost.setAttribute("name", "selenium.host");
        parameterHost.setAttribute("value", "localhost");
        rootElement.appendChild(parameterHost);

        Element parameterPort = doc.createElement("parameter");
        parameterPort.setAttribute("name", "selenium.port");
        parameterPort.setAttribute("value", "3737");
        rootElement.appendChild(parameterPort);

        Element parameterBrowser = doc.createElement("parameter");
        parameterBrowser.setAttribute("name", "selenium.browser");
        parameterBrowser.setAttribute("value", "firefox");
        rootElement.appendChild(parameterBrowser);

        Element parameterUrl = doc.createElement("parameter");
        parameterUrl.setAttribute("name", "selenium.url");
        parameterUrl.setAttribute("value", "http://www.sisgevet.com");
        rootElement.appendChild(parameterUrl);

        Element test = doc.createElement("test");

        Element allTransitions = doc.createElement("Criterio");
        allTransitions.setAttribute("nombre", "All-Transitions");

        StateMachine stateMachine = (StateMachine)modelo.getPackagedElements().get(0);
        Region region = stateMachine.getRegions().get(0);
        Pseudostate estadoInicial = (Pseudostate) region.getSubvertices().stream().filter(s -> s.getClass() == PseudostateImpl.class && ((Pseudostate)s).getKind().getName() == "initial").findFirst().get();

        Map<Integer, Vertex> estados = new HashMap<>();
        Map<Integer, Boolean> visitados = new HashMap<>();
        for (Vertex estado : region.getSubvertices()){
            estados.put(estado.hashCode(), estado);
            visitados.put(estado.hashCode(), false);
        }
        visitados.put(estadoInicial.hashCode(), true);

        Map<Integer, Transition> transiciones = new HashMap<>();
        Map<Integer, Boolean> transicionesVisitadas = new HashMap<>();
        for( Transition transicion : region.getTransitions()){
            transiciones.put(transicion.hashCode(), transicion);
            transicionesVisitadas.put(transicion.hashCode(), false);
        }

        List<NamedElement> pasos = new ArrayList<>();
        pasos.add(estadoInicial);
        List<Transition> transicionesPendientes = new ArrayList<>();
        transicionesPendientes.addAll(estadoInicial.getOutgoings());

        while(!transicionesPendientes.isEmpty()){
            Transition transicion = transicionesPendientes.remove(transicionesPendientes.size()-1);

            Vertex estado = transicion.getTarget();

            pasos.add(transicion);

            transicionesVisitadas.put(transicion.hashCode(), true);
            if (!visitados.get(estado.hashCode())){
                visitados.put(estado.hashCode(), true);
                transicionesPendientes.addAll(estado.getOutgoings());
            }

            if (estado.getClass() == FinalStateImpl.class){
                XMLCreator.nCasos++;
                Element caso = doc.createElement("Caso");
                caso.setAttribute("estadoFinal", estado.getLabel());
                for (NamedElement paso : pasos){
                    if (paso.getClass() == TransitionImpl.class){
                        Element p = doc.createElement("Transicion");
                        p.setAttribute("nombre", paso.getLabel());
                        p.setAttribute("origen", ((Transition)paso).getSource().getLabel());
                        p.setAttribute("destino", ((Transition)paso).getTarget().getLabel());
                        caso.appendChild(p);
                    }
                }
                allTransitions.appendChild(caso);
                pasos.remove(pasos.size()-1);
            } else {
                if (estado.getOutgoings().size() == 0)
                    visitados.put(estado.hashCode(), false);
            }
        }

        Element noVisitadosXml = doc.createElement("TransicionesNoVisitadas");
        for (Integer hashKey : transicionesVisitadas.entrySet().stream().filter(v -> !v.getValue()).map(v -> v.getKey()).collect(Collectors.toList())){
            Transition transicion = transiciones.get(hashKey);
            Element noVisitado = doc.createElement("Transicion");
            noVisitado.setAttribute("nombre", transicion.getLabel());
            noVisitadosXml.appendChild(noVisitado);
        }
        allTransitions.appendChild(noVisitadosXml);

        test.appendChild(allTransitions);
        rootElement.appendChild(test);
        doc.appendChild(rootElement);
        return doc;
    }

    public static Element getCriterioAS(Model modelo, Document doc){
        Element allState = doc.createElement("Criterio");
        allState.setAttribute("nombre", "All-State");

        StateMachine stateMachine = (StateMachine)modelo.getPackagedElements().get(0);
        Region region = stateMachine.getRegions().get(0);

        String regionName = stateMachine.getName();
        allState.setAttribute("diagrama", regionName);
        Pseudostate estadoInicial = (Pseudostate) region.getSubvertices().stream().filter(s -> s.getClass() == PseudostateImpl.class && ((Pseudostate)s).getKind().getName() == "initial").findFirst().get();

        Map<Integer, Vertex> estados = new HashMap<>();
        Map<Integer, Boolean> visitados = new HashMap<>();
        Map<Integer, Boolean> visitadosFinal = new HashMap<>();
        for (Vertex estado : region.getSubvertices()){
            estados.put(estado.hashCode(), estado);
            visitados.put(estado.hashCode(), false);
            visitadosFinal.put(estado.hashCode(), false);

            if (estado.getClass() == StateImpl.class && ((State)estado).getRegions().size() == 1){
                List<Vertex> subEstados = ((State)estado).getRegions().get(0).getSubvertices();
                for (Vertex subEstado : subEstados){
                    estados.put(subEstado.hashCode(), subEstado);
                    visitados.put(subEstado.hashCode(), false);
                    visitadosFinal.put(subEstado.hashCode(), false);
                }
            }
        }
        visitados.put(estadoInicial.hashCode(), true);
        visitadosFinal.put(estadoInicial.hashCode(), true);

        List<Transition> pasos = new ArrayList<>();

        Stack<Transition> transicionesPendientes = new Stack<>();
        transicionesPendientes.addAll(estadoInicial.getOutgoings());

        while (transicionesPendientes.size() > 0) {
            Transition transicion = transicionesPendientes.pop();
            Vertex estado = transicion.getTarget();

            pasos.add(transicion);

            if (!visitados.get(estado.hashCode())){
                visitados.put(estado.hashCode(), true);
                if (estado.getClass() == StateImpl.class && ((State)estado).getRegions().size() == 1){
                    Stack<Transition> subTransiciones = new Stack<>();
                    Vertex subEstadoInicial = ((State)estado).getRegions().get(0).getSubvertices().stream().filter(s -> s.getClass() == PseudostateImpl.class && ((Pseudostate)s).getKind().getName() == "initial").findFirst().get();
                    visitados.put(subEstadoInicial.hashCode(), true);
                    visitadosFinal.put(subEstadoInicial.hashCode(), true);
                    subTransiciones.addAll(subEstadoInicial.getOutgoings());
                    while (subTransiciones.size() > 0){
                        Transition subTransicion = subTransiciones.pop();
                        Vertex subEstadoNext = subTransicion.getTarget();

                        pasos.add(subTransicion);

                        subTransiciones.addAll(subEstadoNext.getOutgoings());
                    }
                }
                transicionesPendientes.addAll(estado.getOutgoings());
            }

            if (estado.getClass() == FinalStateImpl.class){
                XMLCreator.nCasos++;
                Element caso = doc.createElement("Caso");
                caso.setAttribute("estadoFinal", estado.getLabel());
                for (Transition paso : pasos){
                    visitadosFinal.put(paso.getTarget().hashCode(), true);
                    Element p = doc.createElement("Transicion");
                    p.setAttribute("nombre", paso.getLabel());
                    p.setAttribute("origen", paso.getSource().getLabel());
                    p.setAttribute("destino", paso.getTarget().getLabel());
                    caso.appendChild(p);
                }
                allState.appendChild(caso);
                pasos.remove(pasos.size()-1);
            }
        }

        Element noVisitados = doc.createElement("EstadosNoVisitados");
        for (Integer hashKey : visitadosFinal.entrySet().stream().filter(v -> !v.getValue()).map(v -> v.getKey()).collect(Collectors.toList())){
            Vertex estado = estados.get(hashKey);
            Element noVisitado = doc.createElement("Estado");
            noVisitado.setAttribute("nombre", estado.getLabel());
            noVisitados.appendChild(noVisitado);
        }
        allState.appendChild(noVisitados);
        return allState;
    }

    public static Document getCriterioASConcreto(Model modelo){
        CriterioEstados criterioEstados = new CriterioEstados();
        Document doc = criterioEstados.docBuilder.newDocument();

        Element rootElement = doc.createElement("suite");
        rootElement.setAttribute("name", "Test Sisgevet");
        rootElement.setAttribute("verbose", "10");

        /* parameters */
        Element parameterHost = doc.createElement("parameter");
        parameterHost.setAttribute("name", "selenium.host");
        parameterHost.setAttribute("value", "localhost");
        rootElement.appendChild(parameterHost);

        Element parameterPort = doc.createElement("parameter");
        parameterPort.setAttribute("name", "selenium.port");
        parameterPort.setAttribute("value", "3737");
        rootElement.appendChild(parameterPort);

        Element parameterBrowser = doc.createElement("parameter");
        parameterBrowser.setAttribute("name", "selenium.browser");
        parameterBrowser.setAttribute("value", "firefox");
        rootElement.appendChild(parameterBrowser);

        Element parameterUrl = doc.createElement("parameter");
        parameterUrl.setAttribute("name", "selenium.url");
        parameterUrl.setAttribute("value", "http://www.sisgevet.com");
        rootElement.appendChild(parameterUrl);

        Element test = doc.createElement("test");

        Element allState = doc.createElement("Criterio");
        allState.setAttribute("nombre", "All-State");

        StateMachine stateMachine = (StateMachine)modelo.getPackagedElements().get(0);
        Region region = stateMachine.getRegions().get(0);
        Pseudostate estadoInicial = (Pseudostate) region.getSubvertices().stream().filter(s -> s.getClass() == PseudostateImpl.class && ((Pseudostate)s).getKind().getName() == "initial").findFirst().get();

        Map<Integer, Vertex> estados = new HashMap<>();
        Map<Integer, Boolean> visitados = new HashMap<>();
        Map<Integer, Boolean> visitadosFinal = new HashMap<>();
        for (Vertex estado : region.getSubvertices()){
            estados.put(estado.hashCode(), estado);
            visitados.put(estado.hashCode(), false);
            visitadosFinal.put(estado.hashCode(), false);

            if (estado.getClass() == StateImpl.class && ((State)estado).getRegions().size() == 1){
                List<Vertex> subEstados = ((State)estado).getRegions().get(0).getSubvertices();
                for (Vertex subEstado : subEstados){
                    estados.put(subEstado.hashCode(), subEstado);
                    visitados.put(subEstado.hashCode(), false);
                    visitadosFinal.put(subEstado.hashCode(), false);
                }
            }
        }
        visitados.put(estadoInicial.hashCode(), true);
        visitadosFinal.put(estadoInicial.hashCode(), true);

        List<Transition> pasos = new ArrayList<>();

        Stack<Transition> transicionesPendientes = new Stack<>();
        transicionesPendientes.addAll(estadoInicial.getOutgoings());

        while (transicionesPendientes.size() > 0) {
            Transition transicion = transicionesPendientes.pop();
            Vertex estado = transicion.getTarget();

            pasos.add(transicion);

            if (!visitados.get(estado.hashCode())){
                visitados.put(estado.hashCode(), true);
                if (estado.getClass() == StateImpl.class && ((State)estado).getRegions().size() == 1){
                    Stack<Transition> subTransiciones = new Stack<>();
                    Vertex subEstadoInicial = ((State)estado).getRegions().get(0).getSubvertices().stream().filter(s -> s.getClass() == PseudostateImpl.class && ((Pseudostate)s).getKind().getName() == "initial").findFirst().get();
                    visitados.put(subEstadoInicial.hashCode(), true);
                    visitadosFinal.put(subEstadoInicial.hashCode(), true);
                    subTransiciones.addAll(subEstadoInicial.getOutgoings());
                    while (subTransiciones.size() > 0){
                        Transition subTransicion = subTransiciones.pop();
                        Vertex subEstadoNext = subTransicion.getTarget();

                        pasos.add(subTransicion);

                        subTransiciones.addAll(subEstadoNext.getOutgoings());
                    }
                }
                transicionesPendientes.addAll(estado.getOutgoings());
            }

            if (estado.getClass() == FinalStateImpl.class){
                XMLCreator.nCasos++;
                Element caso = doc.createElement("Caso");
                caso.setAttribute("estadoFinal", estado.getLabel());
                for (Transition paso : pasos){
                    visitadosFinal.put(paso.getTarget().hashCode(), true);
                    Element p = doc.createElement("Transicion");
                    p.setAttribute("nombre", paso.getLabel());
                    p.setAttribute("origen", paso.getSource().getLabel());
                    p.setAttribute("destino", paso.getTarget().getLabel());
                    caso.appendChild(p);
                }
                allState.appendChild(caso);
                pasos.remove(pasos.size()-1);
            }
        }

        Element noVisitados = doc.createElement("EstadosNoVisitados");
        for (Integer hashKey : visitadosFinal.entrySet().stream().filter(v -> !v.getValue()).map(v -> v.getKey()).collect(Collectors.toList())){
            Vertex estado = estados.get(hashKey);
            Element noVisitado = doc.createElement("Estado");
            noVisitado.setAttribute("nombre", estado.getLabel());
            noVisitados.appendChild(noVisitado);
        }
        allState.appendChild(noVisitados);
        test.appendChild(allState);
        rootElement.appendChild(test);
        doc.appendChild(rootElement);
        return doc;
    }

    public static Element getCriterioALFP(Model modelo, Document doc){
        Element allLoopFreePath = doc.createElement("Criterio");
        allLoopFreePath.setAttribute("nombre", "All-Loop-Free-Path");

        StateMachine stateMachine = (StateMachine)modelo.getPackagedElements().get(0);
        Region region = stateMachine.getRegions().get(0);
        String regionName = stateMachine.getName();
        allLoopFreePath.setAttribute("diagrama", regionName);
        Pseudostate estadoInicial = (Pseudostate) region.getSubvertices().stream().filter(s -> s.getClass() == PseudostateImpl.class && ((Pseudostate)s).getKind().getName() == "initial").findFirst().get();

        Map<Integer, Vertex> estados = new HashMap<>();
        Map<Integer, Boolean> visitados = new HashMap<>();
        Map<Integer, Boolean> visitadosFinal = new HashMap<>();
        for (Vertex estado : region.getSubvertices()){
            estados.put(estado.hashCode(), estado);
            visitados.put(estado.hashCode(), false);
            visitadosFinal.put(estado.hashCode(), false);
        }
        visitados.put(estadoInicial.hashCode(), true);
        visitadosFinal.put(estadoInicial.hashCode(), true);

        Stack<Transition> pasos = new Stack<>();

        Stack<Transition> transicionesPendientes = new Stack<>();
        transicionesPendientes.addAll(estadoInicial.getOutgoings());

        while (transicionesPendientes.size() > 0){
            Transition transicion = transicionesPendientes.pop();
            if (transicion.getTarget() == transicion.getSource())
                continue;
            Vertex estado = transicion.getTarget();

            if (visitados.get(estado.hashCode())) {
                continue;
            }

            transicionesPendientes.addAll(estado.getOutgoings());

            pasos.add(transicion);

            if (estado.getClass() == FinalStateImpl.class){
                XMLCreator.nCasos++;
                Element caso = doc.createElement("Caso");
                caso.setAttribute("estadoFinal", estado.getLabel());
                for (Transition paso : pasos){
                    visitadosFinal.put(paso.getTarget().hashCode(), true);
                    Element p = doc.createElement("Transicion");
                    p.setAttribute("nombre", paso.getLabel());
                    p.setAttribute("origen", ((Transition)paso).getSource().getLabel());
                    p.setAttribute("destino", ((Transition)paso).getTarget().getLabel());
                    caso.appendChild(p);
                }
                allLoopFreePath.appendChild(caso);
                pasos.pop();
            }
        }
        return allLoopFreePath;
    }

    public static Document getCriterioALFPConcreto(Model modelo){
        CriterioEstados criterioEstados = new CriterioEstados();
        Document doc = criterioEstados.docBuilder.newDocument();
        Element rootElement = doc.createElement("suite");
        rootElement.setAttribute("name", "Test Sisgevet");
        rootElement.setAttribute("verbose", "10");

        /* parameters */
        Element parameterHost = doc.createElement("parameter");
        parameterHost.setAttribute("name", "selenium.host");
        parameterHost.setAttribute("value", "localhost");
        rootElement.appendChild(parameterHost);

        Element parameterPort = doc.createElement("parameter");
        parameterPort.setAttribute("name", "selenium.port");
        parameterPort.setAttribute("value", "3737");
        rootElement.appendChild(parameterPort);

        Element parameterBrowser = doc.createElement("parameter");
        parameterBrowser.setAttribute("name", "selenium.browser");
        parameterBrowser.setAttribute("value", "firefox");
        rootElement.appendChild(parameterBrowser);

        Element parameterUrl = doc.createElement("parameter");
        parameterUrl.setAttribute("name", "selenium.url");
        parameterUrl.setAttribute("value", "http://www.sisgevet.com");
        rootElement.appendChild(parameterUrl);

        Element test = doc.createElement("test");

        Element allLoopFreePath = doc.createElement("Criterio");
        allLoopFreePath.setAttribute("nombre", "All-Loop-Free-Path");

        StateMachine stateMachine = (StateMachine)modelo.getPackagedElements().get(0);
        Region region = stateMachine.getRegions().get(0);
        Pseudostate estadoInicial = (Pseudostate) region.getSubvertices().stream().filter(s -> s.getClass() == PseudostateImpl.class && ((Pseudostate)s).getKind().getName() == "initial").findFirst().get();

        Map<Integer, Vertex> estados = new HashMap<>();
        Map<Integer, Boolean> visitados = new HashMap<>();
        Map<Integer, Boolean> visitadosFinal = new HashMap<>();
        for (Vertex estado : region.getSubvertices()){
            estados.put(estado.hashCode(), estado);
            visitados.put(estado.hashCode(), false);
            visitadosFinal.put(estado.hashCode(), false);
        }
        visitados.put(estadoInicial.hashCode(), true);
        visitadosFinal.put(estadoInicial.hashCode(), true);

        Stack<Transition> pasos = new Stack<>();

        Stack<Transition> transicionesPendientes = new Stack<>();
        transicionesPendientes.addAll(estadoInicial.getOutgoings());

        while (transicionesPendientes.size() > 0){
            Transition transicion = transicionesPendientes.pop();
            if (transicion.getTarget() == transicion.getSource())
                continue;
            Vertex estado = transicion.getTarget();

            if (visitados.get(estado.hashCode())) {
                continue;
            }

            transicionesPendientes.addAll(estado.getOutgoings());

            pasos.add(transicion);

            if (estado.getClass() == FinalStateImpl.class){
                XMLCreator.nCasos++;
                Element caso = doc.createElement("Caso");
                caso.setAttribute("estadoFinal", estado.getLabel());
                for (Transition paso : pasos){
                    visitadosFinal.put(paso.getTarget().hashCode(), true);
                    Element p = doc.createElement("Transicion");
                    p.setAttribute("nombre", paso.getLabel());
                    p.setAttribute("origen", ((Transition)paso).getSource().getLabel());
                    p.setAttribute("destino", ((Transition)paso).getTarget().getLabel());
                    caso.appendChild(p);
                }
                allLoopFreePath.appendChild(caso);
                pasos.pop();
            }
        }
        test.appendChild(allLoopFreePath);
        rootElement.appendChild(test);
        doc.appendChild(rootElement);
        return doc;
    }
}