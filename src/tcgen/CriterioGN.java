package tcgen;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.util.List;

public class CriterioGN {
    private DocumentBuilder docBuilder;

    public CriterioGN(){
        try{
            docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        }
    }

    public static Element getCriterioGN(List<Clase> clases, Document doc){
        Element gn = doc.createElement("Criterio");
        gn.setAttribute("nombre", "GN");

        for (Clase cls : clases) {
            if (cls.getClasesHijas().size() == 0) continue;
            Element criterio = doc.createElement("Clase");
            criterio.setAttribute("nombre", cls.getNombre());

            Element instanciaPadre = Helper.crearInstancia(doc, cls);
            instanciaPadre.setAttribute("tipo", "padre");
            criterio.appendChild(instanciaPadre);

            for (Clase claseHija : cls.getClasesHijas()) {
                XMLCreator.nCasos++;
                Element hija = Helper.crearInstancia(doc, claseHija);
                hija.setAttribute("tipo", "hija");
                for (Metodo metodo : claseHija.getMetodos()){
                    Element met = doc.createElement("Metodo");
                    met.setAttribute("name", metodo.getNombre());
                    met.setAttribute("clase", cls.getNombre());
                    for (Argumento argumento : metodo.getArgumentos()){
                        Element arg = doc.createElement("Argumento");
                        String nombre = argumento.getNombre();
                        String tipo = argumento.getTipo();
                        arg.setAttribute("nombre", nombre);
                        arg.setAttribute("tipo", tipo);
                        arg.setAttribute("valor", Helper.getValor(nombre, tipo));
                        met.appendChild(arg);
                    }
                    hija.appendChild(met);
                }
                criterio.appendChild(hija);
            }
            gn.appendChild(criterio);
        }

        return gn;
    }

    public static Document getCriterioGNConcreto(List<Clase> clases){
        CriterioGN criterioGeneralization = new CriterioGN();
        Document doc = criterioGeneralization.docBuilder.newDocument();
        Element rootElement = doc.createElement("suite");
        rootElement.setAttribute("name", "Test Sisgevet");
        rootElement.setAttribute("verbose", "10");

        /* parameters */
        Element parameterHost = doc.createElement("parameter");
        parameterHost.setAttribute("name", "selenium.host");
        parameterHost.setAttribute("value", "localhost");
        rootElement.appendChild(parameterHost);

        Element parameterPort = doc.createElement("parameter");
        parameterPort.setAttribute("name", "selenium.port");
        parameterPort.setAttribute("value", "3737");
        rootElement.appendChild(parameterPort);

        Element parameterBrowser = doc.createElement("parameter");
        parameterBrowser.setAttribute("name", "selenium.browser");
        parameterBrowser.setAttribute("value", "firefox");
        rootElement.appendChild(parameterBrowser);

        Element parameterUrl = doc.createElement("parameter");
        parameterUrl.setAttribute("name", "selenium.url");
        parameterUrl.setAttribute("value", "http://www.sisgevet.com");
        rootElement.appendChild(parameterUrl);

        Element test = doc.createElement("test");

        for (Clase cls : clases) {
            if (cls.getClasesHijas().size() == 0) continue;
            Element criterio = doc.createElement("Criterio");
            criterio.setAttribute("nombre", "GN");
            criterio.setAttribute("clase", cls.getNombre());

            Element instanciaPadre = Helper.crearInstancia(doc, cls);
            criterio.appendChild(instanciaPadre);

            for (Clase claseHija : cls.getClasesHijas()) {
                XMLCreator.nCasos++;
                Element hija = Helper.crearInstancia(doc, claseHija);
                for (Metodo metodo : claseHija.getMetodos()){
                    Element met = doc.createElement("Metodo");
                    met.setAttribute("name", metodo.getNombre());
                    met.setAttribute("clase", cls.getNombre());
                    for (Argumento argumento : metodo.getArgumentos()){
                        Element arg = doc.createElement("Argumento");
                        String nombre = argumento.getNombre();
                        String tipo = argumento.getTipo();
                        arg.setAttribute("nombre", nombre);
                        arg.setAttribute("tipo", tipo);
                        arg.setAttribute("valor", Helper.getValor(nombre, tipo));
                        met.appendChild(arg);
                    }
                    hija.appendChild(met);
                }
                criterio.appendChild(hija);
            }
            test.appendChild(criterio);
            rootElement.appendChild(test);
        }

        doc.appendChild(rootElement);
        return doc;
    }
}
