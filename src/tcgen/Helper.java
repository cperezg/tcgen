package tcgen;

import org.apache.commons.lang3.RandomUtils;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.internal.impl.*;
import org.eclipse.uml2.uml.resource.UMLResource;
import org.fluttercode.datafactory.impl.DataFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.stream.Collectors;

public class Helper {
    private static DataFactory dataFactory = new DataFactory();

    public static <T> List<List<T>> cartesianProduct(List<ArrayList<Atributo>> lists) {
        List<List<T>> resultLists = new ArrayList<>();
        if (lists.size() == 0) {
            resultLists.add(new ArrayList<>());
            return resultLists;
        } else {
            List<T> firstList = (List<T>) lists.get(0);
            List<List<T>> remainingLists = cartesianProduct(lists.subList(1, lists.size()));
            for (T condition : firstList) {
                for (List<T> remainingList : remainingLists) {
                    List<T> resultList = new ArrayList<>();
                    resultList.add(condition);
                    resultList.addAll(remainingList);
                    resultLists.add(resultList);
                }
            }
        }
        return resultLists;
    }

    public static String getValor(String nombre) {
        if (nombre.toLowerCase().contains("nombre")) {
            return dataFactory.getFirstName();
        } else if (nombre.toLowerCase().contains("apellido")) {
            return dataFactory.getLastName();
        } else if (nombre.toLowerCase().contains("email")) {
            return dataFactory.getEmailAddress();
        } else if (nombre.toLowerCase().contains("telefono")) {
            return "+56-9-" + dataFactory.getNumberBetween(5, 9) + dataFactory.getNumberText(7);
        } else if (nombre.toLowerCase().contains("direccion")) {
            return dataFactory.getAddress() + ", " + dataFactory.getAddressLine2();
        } else if (nombre.toLowerCase().contains("rut")) {
            int rut = dataFactory.getNumberBetween(6000000, 25000000);
            return Integer.toString(rut) + "-" + getDigitoValidador(Integer.toString(rut));
        } else {
            return dataFactory.getRandomText(10, 4);
        }
    }

    public static String getValor(String nombre, String tipo){
        String valor = null;
        switch (tipo) {
            case "String":
            case "EString":
                valor = getValor(nombre);
                break;
            case "int":
            case "EInt":
                valor = Integer.toString(RandomUtils.nextInt(0, Integer.MAX_VALUE));
                break;
            case "float":
            case "double":
            case "EFloat":
            case "EDouble":
                valor = Float.toString(RandomUtils.nextFloat(0, Float.MAX_VALUE));
                break;
            case "EDate":
                valor = getRandomDate();
                break;
            case "boolean":
            case "EBoolean":
                valor = Boolean.toString(RandomUtils.nextInt(0, 2) % 2 == 0);
                break;
            default:
                valor = "";
                break;
        }
        return valor;
    }

    public static String getRandomDate() {
        GregorianCalendar gc = new GregorianCalendar();
        int year = RandomUtils.nextInt(1900, 2016);

        gc.set(gc.YEAR, year);

        int dayOfYear = RandomUtils.nextInt(1, gc.getActualMaximum(gc.DAY_OF_YEAR));

        gc.set(gc.DAY_OF_YEAR, dayOfYear);

        return gc.get(gc.YEAR) + "-" + (gc.get(gc.MONTH) + 1) + "-" + gc.get(gc.DAY_OF_MONTH);
    }

    public static Element crearInstancia(Document doc, Clase clase){
        Element element = doc.createElement("Clase");
        element.setAttribute("nombre", clase.getNombre());

        for (Atributo atributo : clase.getAtributos()){
            Element attr = doc.createElement("Atributo");
            attr.setAttribute("nombre", atributo.getNombre());
            attr.setAttribute("tipo", atributo.getTipo());
            attr.setAttribute("valor", Helper.getValor(atributo.getNombre(), atributo.getTipo()));
            element.appendChild(attr);
        }
        return element;
    }

    public static Model getModel(String pathToModel) {
        URI typesUri = URI.createFileURI(pathToModel);
        ResourceSet set = new ResourceSetImpl();

        set.getPackageRegistry().put(UMLPackage.eNS_URI, UMLPackage.eINSTANCE);
        set.getResourceFactoryRegistry().getExtensionToFactoryMap().put(UMLResource.FILE_EXTENSION, UMLResource.Factory.INSTANCE);
        set.createResource(typesUri);
        Resource r = set.getResource(typesUri, true);

        Model m = (Model) EcoreUtil.getObjectByType(r.getContents(), UMLPackage.Literals.MODEL);

        return m;
    }

    public static List<Clase> getClases(Model m){
        List<Clase> clases = new ArrayList<>();
        PackageImpl paquete = null;
        for (PackageImpl paquetito : m.getOwnedElements().stream().filter(p -> p.getClass() == PackageImpl.class).map(c -> (PackageImpl) c).collect(Collectors.toList())) {
            paquete = paquetito;
        }
        System.out.println("Package: " + paquete.getLabel());
        for (ClassImpl cls : paquete.getOwnedElements().stream().filter(p -> p.getClass() == ClassImpl.class).map(c -> (ClassImpl) c).collect(Collectors.toList())) {
            System.out.println("Clase: " + cls.getLabel());

            Clase clase = new Clase();
            clase.setNombre(cls.getLabel());

            /* Atributos */
            List<Atributo> atributos = new ArrayList<>();
            System.out.println("  - Atributos");
            for (PropertyImpl propiedad : cls.getAllAttributes().stream().map(c -> (PropertyImpl) c).collect(Collectors.toList())) {
                String tipo = null;
                if (propiedad.getType().toString().split("#").length > 1) {
                    tipo = propiedad.getType().toString().split("#")[1];
                    tipo = tipo.substring(0, tipo.length() - 1);
                } else
                    tipo = propiedad.getType().getLabel();

                System.out.println("  * " + tipo + " " + propiedad.getLabel());
                atributos.add(new Atributo(propiedad.getLabel(), tipo));
            }
            clase.setAtributos(atributos);

            /* Operaciones */
            List<Metodo> metodos = new ArrayList<>();
            System.out.println("  - Operaciones");
            for (OperationImpl operacion : cls.getAllOperations().stream().map(c -> (OperationImpl) c).collect(Collectors.toList())) {
                System.out.print("  * " + operacion.getLabel() + "(");
                int orden = 0;
                List<Argumento> argumentos = new ArrayList<>();
                for (ParameterImpl parametro : operacion.getMembers().stream().map(c -> (ParameterImpl) c).filter(c -> c.getDirection().getName().equals("in")).collect(Collectors.toList())) {
                    String tipo = new String();
                    if (parametro.getType().toString().split("#").length > 1) {
                        tipo = parametro.getType().toString().split("#")[1];
                        tipo = tipo.substring(0, tipo.length() - 1);
                    } else
                        tipo = parametro.getType().getLabel();
                    System.out.print(tipo + " " + parametro.getLabel());
                    if (parametro != operacion.getMembers().get(operacion.getMembers().size() - 1)) {
                        System.out.print(", ");
                    }
                    argumentos.add(new Argumento(parametro.getLabel(), tipo, orden++));
                }
                String retorno = "void";
                List<Parameter> params = operacion.getMembers().stream().map(c -> (Parameter)c).filter(c -> c.getDirection().getName().equals("return")).collect(Collectors.toList());
                if (params.size() > 0){
                    if (params.get(0).getType().toString().split("#").length > 1) {
                        retorno = params.get(0).getType().toString().split("#")[1];
                        retorno = retorno.substring(0, retorno.length() - 1);
                    } else
                        retorno = params.get(0).getType().getLabel();
                }
                System.out.println(") : " + retorno);
                metodos.add(new Metodo(retorno, operacion.getLabel(), argumentos));
            }
            clase.setMetodos(metodos);

            // Generalizacion
            System.out.println("  - Generalizaciones");
            for (GeneralizationImpl generalization : cls.getGeneralizations().stream().map(c -> (GeneralizationImpl) c).collect(Collectors.toList())) {
                ClassImpl clasePadre = (ClassImpl) generalization.getGeneral();
                Clase clsPadre = (Clase) clases.stream().filter(x -> x.getNombre() == clasePadre.getLabel()).collect(Collectors.toList()).get(0);
                clsPadre.addClaseHija(clase);
                System.out.println("  * " + clasePadre.getLabel());
            }
            clases.add(clase);
            System.out.println();
        }
        System.out.println("Asociaciones");
        for (AssociationImpl asociacion : m.getPackagedElements().stream().filter(p -> p.getClass() == AssociationImpl.class).map(c -> (AssociationImpl) c).collect(Collectors.toList())) {
            List<PropertyImpl> clasesAsociacion = asociacion.getOwnedElements().stream().map(c -> (PropertyImpl) c).collect(Collectors.toList());

            ClassImpl claseDesde = (ClassImpl) clasesAsociacion.get(0).getType();
            ClassImpl claseHasta = (ClassImpl) clasesAsociacion.get(1).getType();

            PropertyImpl propiedad1 = clasesAsociacion.get(0);
            PropertyImpl propiedad2 = clasesAsociacion.get(1);

            String multiDesde = propiedad1.getUpper() == -1 ? "*" : Integer.toString(propiedad1.getUpper());

            String multiHasta = propiedad2.getUpper() == -1 ? "*" : Integer.toString(propiedad2.getUpper());

            System.out.println(claseDesde.getLabel() + " " + multiDesde + " --> " + multiHasta + " " + claseHasta.getLabel());

            Clase origen = null;
            for (Clase cls : clases) {
                if (cls.getNombre().contains(claseDesde.getLabel()))
                    origen = cls;
            }
            if (origen != null) {
                Clase clase = (clases.stream().filter(c -> c.getNombre() == claseHasta.getLabel()).collect(Collectors.toList()).get(0));
                origen.getAsociaciones().add(new Asociacion(clase, propiedad1.getUpper(), propiedad2.getUpper()));
            }
        }
        return clases;
    }

    public static String getDigitoValidador(String vrut)
    {
        String rut = vrut.trim();

        int cantidad = rut.length();
        int factor = 2;
        int suma = 0;
        String verificador = "";

        for(int i = cantidad; i > 0; i--)
        {
            if(factor > 7)
            {
                factor = 2;
            }
            suma += (Integer.parseInt(rut.substring((i-1), i)))*factor;
            factor++;

        }
        verificador = String.valueOf(11 - suma%11);
        if(verificador.equals("10"))
        {
            return "k";
        }
        else
        {
            if(verificador.equals("11"))
            {
                return "0";
            }
            else
            {
                return verificador;
            }
        }
    }

}
