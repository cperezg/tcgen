package tcgen;

import java.util.List;
public class Metodo {
    private String tipoRetorno;
    private String nombre;
    private List<Argumento> argumentos;

    public Metodo(String tipoRetorno, String nombre, List<Argumento> argumentos) {
        this.tipoRetorno = tipoRetorno;
        this.nombre = nombre;
        this.argumentos = argumentos;
    }

    public List<Argumento> getArgumentos() {
        return argumentos;
    }

    public void setArgumentos(List<Argumento> argumentos) {
        this.argumentos = argumentos;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipoRetorno() {
        return tipoRetorno;
    }

    public void setTipoRetorno(String tipoRetorno) {
        this.tipoRetorno = tipoRetorno;
    }

}
