package tcgen;

/**
 * Created by JuanPa on 07-Apr-16.
 */
public enum Tipo {
    INTEGER ("Integer"),
    STRING ("String"),
    DOUBLE ("Double"),
    FLOAT ("Float"),
    BOOLEAN ("Boolean"),
    CHAR ("Char"),
    DATE ("Date"),
    DATETIME ("Datetime");

    private Tipo(String tipo){
        this.tipo = tipo;
    }

    private final String tipo;

    public String getTipo(){return tipo;}
}
